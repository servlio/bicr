use chrono::{NaiveDate, NaiveTime, ParseError};
use std::path::PathBuf;
use structopt::StructOpt;
use tracing::Level;

mod display;
pub mod handlers;

/// Parse an incoming date in the current locale's format
fn parse_date(incoming: &str) -> Result<NaiveDate, ParseError> {
    NaiveDate::parse_from_str(incoming, "%x")
}

/// Parse an incoming time in your local timezone the current locale's format
fn parse_time(incoming: &str) -> Result<NaiveTime, ParseError> {
    NaiveTime::parse_from_str(incoming, "%X")
}

#[derive(Debug, StructOpt)]
#[structopt(
    name = "bicr",
    about = "A simple container daemon for running single binaries in an image",
    author = "Alex Krantz <alex@krantz.dev>"
)]
pub struct Args {
    /// The global minimum level to log at (unused in daemon mode)
    #[structopt(short, long, default_value = "info")]
    pub log_level: Level,

    /// The host and port that BiCR can be accessed on (unused in daemon mode)
    #[structopt(short, long, default_value = "http://127.0.0.1:2427")]
    pub address: String,

    #[structopt(subcommand)]
    pub cmd: Subcommand,
}

#[derive(Debug, StructOpt)]
pub enum Subcommand {
    /// Run the engine as a daemon
    Daemon {
        /// Path to the configuration file, defaults to 'config.toml' in the current directory.
        #[structopt(short, long, env = "BICR_CONFIG")]
        config: Option<PathBuf>,
    },
    /// Create a new container running the provided command and based on a certain image.
    Create {
        #[structopt(subcommand)]
        location: CreateFromLocations,
    },
    /// Start/restart a container by its ID.
    Start(IdOnly),
    /// Get the current status of a container by its ID.
    Status(IdOnly),
    /// Get the current runtime statistics about a container including CPU, memory, network, and disk usage.
    Statistics(IdOnly),
    /// Get all the logs from a container by its ID. A timeframe can be specified for what logs are gathered using the start and/or end time.
    Logs {
        /// The ID of the container.
        #[structopt(name = "ID")]
        id: String,

        /// The furthest date logs will be pulled from.
        #[structopt(long, name = "START_DATE", parse(try_from_str = parse_date))]
        start_date: Option<NaiveDate>,

        /// The furthest time logs will be pulled from on the given date or the current date if not specified.
        #[structopt(long, name = "START_TIME", parse(try_from_str = parse_time))]
        start_time: Option<NaiveTime>,

        /// The earliest date logs will be pulled from.
        #[structopt(long, name = "END_DATE", parse(try_from_str = parse_date))]
        end_date: Option<NaiveDate>,

        /// The earliest time logs will be pulled from on the given date or the current date if not specified.
        #[structopt(long, name = "END_TIME", parse(try_from_str = parse_time))]
        end_time: Option<NaiveTime>,
    },
    /// Stop a running container by its ID. If the container is not stopped gracefully after 3 seconds, it will be killed.
    Stop(IdOnly),
    /// Kill a running container by its ID using SIGTERM.
    Kill(IdOnly),
    /// Delete a container from disk by its ID.
    Delete(IdOnly),
}

#[derive(Debug, StructOpt)]
pub enum CreateFromLocations {
    /// Copy a binary from the server's filesystem.
    ///
    /// NOTE: the binary is copied from the server's filesystem into the container, not
    /// from your local filesystem to the server.
    Local(CreateBase),
    /// Pull the binary to run from a AWS S3 bucket or S3-compatible service.
    S3 {
        /// The endpoint for the S3 compatible API. Defaults to the AWS S3 API.
        #[structopt(short, long)]
        endpoint: Option<String>,

        /// The name of the bucket to pull from
        #[structopt(name = "BUCKET")]
        bucket: String,

        /// The region that the bucket is in
        #[structopt(name = "REGION")]
        region: String,

        #[structopt(flatten)]
        base: CreateBase,
    },
    /// Pull the binary to run from a Google Cloud Storage bucket.
    GCS {
        /// The name of the bucket to pull from
        #[structopt(name = "BUCKET")]
        bucket: String,

        #[structopt(flatten)]
        base: CreateBase,
    },
}

#[derive(Debug, StructOpt)]
pub struct CreateBase {
    /// The name and path where the copied/downloaded file should be sent in the container.
    /// The binary must be referenced by the name at the end of the path.
    /// For example, with the default value, you'd use `entrypoint --help` to run the command.
    #[structopt(short, long, default_value = "/usr/local/bin/entrypoint")]
    pub destination: String,

    /// The image to use as a base
    #[structopt(name = "IMAGE")]
    pub image: String,

    /// The absolute path to the binary to copy into the image.
    #[structopt(name = "BINARY")]
    pub binary: String,

    /// The command with arguments to execute. If your command has extra flags
    /// associated with it, add `--` after the image name to interpret it as part
    /// of the command.
    #[structopt(name = "COMMAND")]
    pub command: Vec<String>,
}

#[derive(Debug, StructOpt)]
pub struct IdOnly {
    /// The ID of the container. This is a random, 16-character alphanumeric value.
    #[structopt(name = "ID")]
    pub id: String,
}
