use crate::protobuf::runtime::{
    log::Stdio, GenericResponse, LogsResponse, State, StatisticsResponse,
};
use tonic::{Code, Status};

static UNITS: [char; 4] = ['T', 'G', 'M', 'K'];

/// Format some number of bytes as a human readable value
fn human_readable_bytes(size: u64) -> String {
    for (i, u) in UNITS.iter().enumerate() {
        let marker = 1024u64.pow((UNITS.len() - i) as u32);
        if size > marker {
            return if size / marker < 10 {
                format!("{:.1}{}", (size as f32 / marker as f32), u)
            } else {
                format!("{}{}", (size / marker), u)
            };
        }
    }
    format!("{}B", size)
}

/// Display a error message to the console
pub fn error(status: Status) {
    // Display error prefix
    match status.code() {
        Code::Unknown => print!("An unknown error occurred: "),
        Code::InvalidArgument => print!("Invalid argument: "),
        Code::NotFound => print!("Not found: "),
        Code::FailedPrecondition => print!("Invalid precondition: "),
        Code::Aborted => print!("Execution aborted: "),
        Code::Internal => print!("An internal error occurred: "),
        _ => {
            println!(
                "An unused error occurred (this should never happen): {}",
                status
            );
            return;
        }
    }

    println!("{}", status.message());
}

/// Display the generic response to the console
pub fn generic_response(response: GenericResponse, display_id: bool) {
    let state = State::from_i32(response.state).expect("failed to decode response state");
    match state {
        State::Created => print!("Created"),
        State::Running => print!("Running"),
        State::Stopped => print!("Stopped"),
    }

    // Optionally display the container's id
    if display_id {
        println!(" {}", response.id);
    }
}

/// Display the statistics response to the console
pub fn statistics(response: StatisticsResponse) {
    // Create headers
    let mut table = table!(["Name", "CPU %", "Mem %", "Net I/O", "Block I/O", "PIDs"]);

    // Net i/o calculation
    let mut net_input = 0;
    let mut net_output = 0;
    for (_, net) in response.network.iter() {
        net_input += net.rx_bytes;
        net_output += net.tx_bytes;
    }

    // Block i/o calculation
    let mut block_in = 0;
    let mut block_out = 0;
    for block in response.blk_io.io_service_bytes_recursive.iter() {
        match block.op.as_ref() {
            "Read" => block_out += block.value,
            "Write" => block_in += block.value,
            _ => continue,
        }
    }

    // Create sub-tables for network and block io
    let net_table = table!(
        ["In", "Out"],
        [
            human_readable_bytes(net_input),
            human_readable_bytes(net_output)
        ]
    );
    let block_io_table = table!(
        ["In", "Out"],
        [
            human_readable_bytes(block_in),
            human_readable_bytes(block_out)
        ]
    );

    // Round the percents to 2 decimals
    let rounded_cpu = format!("{:.2}", response.cpu.cpu);
    let rounded_memory = format!(
        "{:.2}",
        response.memory.usage.unwrap_or_default() as f64
            / response.memory.limit.unwrap_or_default() as f64,
    );

    // Add statistics
    table.add_row(row![
        response.id,
        rounded_cpu,
        rounded_memory,
        net_table,
        block_io_table,
        response.pids.current,
    ]);

    // Print the table
    table.printstd();
}

/// Display the logs response to the console
pub fn logs(response: LogsResponse) {
    for log in response.logs {
        // Decode the device type
        let device = match log.r#type() {
            Stdio::Stdout => "OUT",
            Stdio::Stderr => "ERR",
            Stdio::Unknown => "UNK",
        };

        // Print the result
        println!("{} | {}", device, log.content);
    }
}
