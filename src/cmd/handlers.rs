use super::display;
use crate::protobuf::runtime::{
    binary_source::Location, runtime_client::RuntimeClient, BinarySource, CreateRequest, IdRequest,
    LogsRequest,
};
use anyhow::{Context, Result};
use chrono::{DateTime, NaiveDate, NaiveDateTime, NaiveTime, Utc};
use prost_types::Timestamp;
use tonic::{transport::Channel, Request};

macro_rules! display_if_error {
    ( $x:expr ) => {
        match $x {
            Ok(v) => v,
            Err(status) => {
                display::error(status);
                return Ok(());
            }
        }
    };
}

/// Generate a client connection from an address
async fn client(address: String) -> Result<RuntimeClient<Channel>> {
    Ok(RuntimeClient::connect(address)
        .await
        .context("failed to connect to remote")?)
}

/// Create a UTC datetime from the date and time components in local time.
/// If the date or time is not provided, the current time or date will be substituted.
fn timestamp_from_parts(date: Option<NaiveDate>, time: Option<NaiveTime>) -> Option<Timestamp> {
    if date.is_none() && time.is_none() {
        None
    } else {
        // Get the current timestamp
        let now = Utc::now().naive_local();

        // Create a full timestamp, substituting the current date and time if needed
        let date_time = NaiveDateTime::new(date.unwrap_or(now.date()), time.unwrap_or(now.time()));

        // Convert to UTC
        let utc = DateTime::<Utc>::from_utc(date_time, Utc);

        Some(Timestamp {
            seconds: utc.timestamp(),
            nanos: utc.timestamp_subsec_nanos() as i32,
        })
    }
}

/// Create a new container with the given image and command
pub async fn create(
    address: String,
    pull_from: Location,
    image: String,
    destination: String,
    path: String,
    command: Vec<String>,
    bucket: Option<String>,
    region: Option<String>,
    endpoint: Option<String>,
) -> Result<()> {
    let mut client = client(address).await?;

    // Issue the request
    let response = display_if_error!(
        client
            .create(Request::new(CreateRequest {
                binary: BinarySource {
                    pull_from: pull_from.into(),
                    destination,
                    path,
                    bucket,
                    region,
                    endpoint,
                },
                command,
                image
            }))
            .await
    );

    // Display the response
    display::generic_response(response.into_inner(), true);

    Ok(())
}

/// Start a stopped or initialized container
pub async fn start(address: String, id: String) -> Result<()> {
    let mut client = client(address).await?;

    // Issue the request
    let response = display_if_error!(client.start(Request::new(IdRequest { id })).await);

    // Display the response
    display::generic_response(response.into_inner(), true);

    Ok(())
}

/// Stop a running container
pub async fn stop(address: String, id: String) -> Result<()> {
    let mut client = client(address).await?;

    // Issue the request
    let response = display_if_error!(client.stop(Request::new(IdRequest { id })).await);

    // Display the response
    display::generic_response(response.into_inner(), true);

    Ok(())
}

/// Get the current status of a container
pub async fn status(address: String, id: String) -> Result<()> {
    let mut client = client(address).await?;

    // Issue the request
    let response = display_if_error!(client.status(Request::new(IdRequest { id })).await);

    // Display the response
    display::generic_response(response.into_inner(), false);

    Ok(())
}

/// Get the current runtime statistics of a container
pub async fn statistics(address: String, id: String) -> Result<()> {
    let mut client = client(address).await?;

    // Issue the request
    let response = display_if_error!(client.statistics(Request::new(IdRequest { id })).await);

    // Display the response
    display::statistics(response.into_inner());

    Ok(())
}

/// Get the logs in the specified timeframe for a container
pub async fn logs(
    address: String,
    id: String,
    start_date: Option<NaiveDate>,
    start_time: Option<NaiveTime>,
    end_date: Option<NaiveDate>,
    end_time: Option<NaiveTime>,
) -> Result<()> {
    let mut client = client(address).await?;

    // Create the start and timestamp
    let start = timestamp_from_parts(start_date, start_time);
    let end = timestamp_from_parts(end_date, end_time);

    // Issue the request
    let response = display_if_error!(
        client
            .logs(Request::new(LogsRequest { id, start, end }))
            .await
    );

    // Display the response
    display::logs(response.into_inner());

    Ok(())
}

/// Delete the specified container from disk
pub async fn delete(address: String, id: String) -> Result<()> {
    let mut client = client(address).await?;

    // Issue request
    display_if_error!(
        client
            .delete(Request::new(IdRequest { id: id.clone() }))
            .await
    );
    println!("Deleted {}", id);

    Ok(())
}

/// Forcibly stop a container with SIGTERM
pub async fn kill(address: String, id: String) -> Result<()> {
    let mut client = client(address).await?;

    // Issue request
    display_if_error!(
        client
            .kill(Request::new(IdRequest { id: id.clone() }))
            .await
    );
    println!("Killed {}", id);

    Ok(())
}
