use anyhow::{Context, Result};
use btrfsutil::{subvolume::Subvolume, BtrfsUtilError};
use serde::{de::Error, Deserialize, Deserializer};
use std::{collections::HashMap, net::SocketAddr, path::PathBuf, str::FromStr};
use tokio::{fs::File, io::AsyncReadExt};
use tracing::Level;

/// Parse the specified configuration file
pub async fn parse(path: Option<PathBuf>) -> Result<Config> {
    // Open the file
    let mut file = match path {
        Some(p) => File::open(&p)
            .await
            .with_context(|| format!("Configuration file '{:?}' does not exist", p))?,
        None => File::open("config.toml")
            .await
            .context("Configuration file './config.toml' does not exist")?,
    };

    // Read the file contents
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .await
        .context("Unable to read the file")?;

    // Parse the config
    Ok(toml::from_str::<Config>(&contents).context("Failed to parse the configuration")?)
}

/// Deserialize a logging level from a string
fn deserialize_level<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Level, D::Error> {
    let s = String::deserialize(deserializer)?;
    Level::from_str(&s).map_err(D::Error::custom)
}

/// Root representation of the configuration file
#[derive(Clone, Debug, Deserialize)]
pub struct Config {
    pub containers: Containers,
    #[serde(default)]
    pub logging: Logging,
    #[serde(default)]
    pub transport: Transport,
    #[serde(default)]
    pub podman: Podman,
}

impl Config {
    /// Ensure the passed configuration is valid
    pub fn validate(&self) -> Result<()> {
        // Ensure containers storage exists
        if !self.containers.path.exists() || !self.containers.path.is_dir() {
            return Err(anyhow!(
                "Container storage path does not exist or is not a directory"
            ));
        }

        // Ensure containers storage is on a btrfs filesystem
        // (essentially checking that subvolumes can be sent here)
        Subvolume::get(&self.containers.path).map_err(Config::convert_btrfs_error)?;

        // Ensure there is a mapping of images
        if self.containers.images.is_empty() {
            return Err(anyhow!("Cannot run without images to use"));
        }

        // Ensure all images are valid paths and on a btrfs filesystem
        for (name, unvalidated) in self.containers.images.iter() {
            if !unvalidated.exists() || !unvalidated.is_dir() {
                return Err(anyhow!(
                    "Image '{}' cannot be found at {:?}",
                    name,
                    unvalidated
                ));
            }

            Subvolume::get(unvalidated).map_err(Config::convert_btrfs_error)?;
        }

        Ok(())
    }

    /// Convert a btrfs error into a human readable error message
    fn convert_btrfs_error(e: BtrfsUtilError) -> anyhow::Error {
        match e {
            BtrfsUtilError::InoLookupFailed
            | BtrfsUtilError::InoLookupUserFailed
            | BtrfsUtilError::NotBtrfs => {
                anyhow!("Path does not point to a btrfs filesystem or subvolume.")
            }
            e => anyhow!("An expected btrfs error occurred: {}", e),
        }
    }
}

/// Configuration for logging
#[derive(Clone, Debug, Deserialize)]
pub struct Logging {
    /// The minimum level to log
    #[serde(deserialize_with = "deserialize_level", default = "default_log_level")]
    pub level: Level,

    /// Optionally output in JSON
    #[serde(default)]
    pub json: bool,
}

impl Default for Logging {
    fn default() -> Self {
        Logging {
            level: Level::INFO,
            json: false,
        }
    }
}

/// gRPC transport configuration
#[derive(Clone, Debug, Deserialize)]
pub struct Transport {
    /// The address and port string to listen on
    #[serde(default = "default_transport_address")]
    pub address: SocketAddr,

    /// TLS configuration
    #[serde(default)]
    pub tls: TransportTLS,
}

impl Default for Transport {
    fn default() -> Self {
        Transport {
            address: SocketAddr::from_str("127.0.0.1:2427")
                .expect("failed to parse default address"),
            tls: TransportTLS::default(),
        }
    }
}

/// Configuration for gRPC over TLS
#[derive(Clone, Debug, Deserialize)]
pub struct TransportTLS {
    /// Whether or not to use TLS
    #[serde(default)]
    pub enabled: bool,

    /// The certificate private key
    #[serde(default)]
    pub key: String,

    /// The certificate public key
    #[serde(default)]
    pub cert: String,
}

impl Default for TransportTLS {
    fn default() -> Self {
        TransportTLS {
            enabled: false,
            key: "".into(),
            cert: "".into(),
        }
    }
}

/// Podman configuration
#[derive(Clone, Debug, Deserialize)]
pub struct Podman {
    /// Path to the podman socket binary
    #[serde(default)]
    pub path: PathBuf,
}

impl Default for Podman {
    fn default() -> Self {
        Podman {
            path: PathBuf::from("/run/podman/podman.sock"),
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct Containers {
    /// Path to where container data should be stored
    pub path: PathBuf,

    /// Mapping of image names to paths
    pub images: HashMap<String, PathBuf>,
}

/// The default minimum level to log at
fn default_log_level() -> Level {
    Level::INFO
}

/// The default socket address
fn default_transport_address() -> SocketAddr {
    SocketAddr::from_str("127.0.0.1:2427").expect("failed to parse default address")
}
