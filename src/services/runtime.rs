use crate::{
    config::Config,
    podman::{ContainerStatus, IntoProtobuf, Podman, Spec},
    protobuf::runtime::{
        binary_source::Location, runtime_server::Runtime, CreateRequest, EmptyResponse,
        GenericResponse, IdRequest, LogsRequest, LogsResponse, StatisticsResponse,
    },
};
use awsregion::Region;
use btrfsutil::subvolume::Subvolume;
use rand::{distributions::Alphanumeric, Rng};
use s3::{bucket::Bucket, creds::Credentials};
use std::{
    collections::HashMap, fs::Permissions, os::unix::fs::PermissionsExt, path::PathBuf,
    process::Stdio,
};
use tokio::fs::File;
use tokio::{
    fs::{copy, remove_dir_all},
    process::Command,
};
use tonic::{Request, Response, Status};
use users::get_effective_uid;

#[derive(Debug)]
pub struct RuntimeService {
    client: Podman,
    // TODO: replace with btrfsutils::subvolume::Subvolume
    images: HashMap<String, PathBuf>,
    containers: PathBuf,
    rootless: bool,
}

impl RuntimeService {
    pub fn new(config: &Config) -> Self {
        // Join image paths with parent path
        let mut images = HashMap::new();
        for (key, value) in config.containers.images.iter() {
            images.insert(key.to_string(), Subvolume::get(value).unwrap());
        }

        RuntimeService {
            client: Podman::new(&config.podman.path),
            containers: PathBuf::from(&config.containers.path),
            images: config.containers.images.clone(),
            rootless: get_effective_uid() != 0,
        }
    }

    /// Copy a local binary into the container's file system
    async fn local_copy_into_container<P: Into<PathBuf>>(
        &self,
        id: &str,
        source: P,
        destination: P,
    ) -> Result<(), Status> {
        let source = source.into();
        let destination = destination.into();

        // Remove the starting forward slash if it exists
        let destination = if destination.starts_with("/") {
            destination.strip_prefix("/").unwrap().to_path_buf()
        } else {
            destination
        };

        // Ensure the source exists
        if !source.exists() {
            return Err(Status::invalid_argument("source binary does not exist"));
        }

        // Ensure the path within the container exists
        let container_destination = self.containers.join(id).join(destination);
        if !container_destination.parent().unwrap().exists() {
            return Err(Status::invalid_argument(
                "containing folder for destination binary does not exist",
            ));
        }

        // Copy the binary into the container
        copy(source, container_destination).await.map_err(|e| {
            error!("failed to copy file into container: {}", e);
            Status::internal("failed to copy file")
        })?;

        Ok(())
    }

    /// Download a binary from AWS S3 or an S3-compatible service into the container's file system
    async fn s3_copy_into_container<S: Into<PathBuf>>(
        &self,
        id: &str,
        path: String,
        destination: S,
        bucket: Option<String>,
        region: Option<String>,
        endpoint: Option<String>,
    ) -> Result<(), Status> {
        // Ensure required arguments passed
        if bucket.is_none() {
            return Err(Status::invalid_argument(
                "the bucket containing the file must be provided",
            ));
        } else if region.is_none() {
            return Err(Status::invalid_argument(
                "the bucket's region must be provided",
            ));
        }

        // Remove the starting forward slash if it exists
        let destination = destination.into();
        let destination = if destination.starts_with("/") {
            destination.strip_prefix("/").unwrap().to_path_buf()
        } else {
            destination
        };

        // Ensure the path within the container exists
        let container_destination = self.containers.join(id).join(destination);
        if !container_destination.parent().unwrap().exists() {
            return Err(Status::invalid_argument(
                "containing folder for destination binary does not exist",
            ));
        }

        // Parse the region string depending on whether a custom endpoint is provided
        let region = if let Some(endpoint) = endpoint {
            Region::Custom {
                region: region.unwrap(),
                endpoint,
            }
        } else {
            region.unwrap().parse::<Region>().unwrap()
        };
        if region.endpoint() == format!("{}", region) {
            return Err(Status::invalid_argument("unrecognized region"));
        }

        // Authenticate anonymously
        // TODO: support authenticated requests
        let credentials = Credentials::anonymous().unwrap();

        // Connect to the bucket
        let bucket = Bucket::new(&bucket.unwrap(), region, credentials).unwrap();

        // Create the output file
        let mut out = File::create(container_destination)
            .await
            .map_err(|_| Status::internal("failed to create output file"))?;
        out.set_permissions(Permissions::from_mode(0o755))
            .await
            .map_err(|_| Status::internal("failed to set file permissions"))?;

        // Stream the file from the bucket
        // TODO: improve error handling when file not found
        bucket
            .tokio_get_object_stream(path, &mut out)
            .await
            .map_err(|_| Status::internal("failed to download file"))?;

        Ok(())
    }

    /// Download a binary from Google Cloud Storage into the container's file system
    async fn gcs_copy_into_container<S: Into<PathBuf>>(
        &self,
        _id: &str,
        _path: String,
        _destination: S,
        _bucket: Option<String>,
    ) -> Result<(), Status> {
        Err(Status::unimplemented(
            "pulling from GCS not yet implemented",
        ))
    }
}

#[tonic::async_trait]
impl Runtime for RuntimeService {
    /// Handle creating a new container
    #[tracing::instrument]
    async fn create(
        &self,
        request: Request<CreateRequest>,
    ) -> Result<Response<GenericResponse>, Status> {
        // Get the inner request data
        let create_request = request.into_inner();

        // Ensure there is a command
        if create_request.command.len() == 0 {
            return Err(Status::invalid_argument("a command must be provided"));
        }

        // Retrieve the image volume
        let volume = match self.images.get(&create_request.image) {
            Some(v) => v,
            None => return Err(Status::invalid_argument("specified image does not exist")),
        };

        // Generate an id for the container
        let id: String = rand::thread_rng()
            .sample_iter(Alphanumeric)
            .take(32)
            .collect();

        // Construct directory path for container filesystem
        let container_path = self.containers.join(&id);

        // Create a snapshot of the image
        // This is a temporary hack pending response from https://github.com/cezarmathe/btrfsutil-rs/issues/8
        if !Command::new("/usr/local/bin/btrfs")
            .arg("subvol")
            .arg("snapshot")
            .arg(volume)
            .arg(&container_path)
            .stdout(Stdio::null())
            .stdin(Stdio::null())
            .status()
            .await
            .map_err(|_| Status::aborted("failed to create snapshot"))?
            .success()
        {
            return Err(Status::aborted("failed to create snapshot"));
        }

        // Convert an integer to the location enum
        if !Location::is_valid(create_request.binary.pull_from) {
            return Err(Status::invalid_argument("invalid pull from location"));
        }

        // Copy the binary into the container
        match Location::from_i32(create_request.binary.pull_from).unwrap() {
            Location::Local => {
                self.local_copy_into_container(
                    &id,
                    create_request.binary.path,
                    create_request.binary.destination,
                )
                .await?;
            }
            Location::S3 => {
                self.s3_copy_into_container(
                    &id,
                    create_request.binary.path,
                    create_request.binary.destination,
                    create_request.binary.bucket,
                    create_request.binary.region,
                    create_request.binary.endpoint,
                )
                .await?
            }
            Location::Gcs => {
                self.gcs_copy_into_container(
                    &id,
                    create_request.binary.path,
                    create_request.binary.destination,
                    create_request.binary.bucket,
                )
                .await?
            }
        };

        // Generate a new image specification
        let spec = Spec::generate(create_request.command, &id, &container_path);

        // Create the container
        self.client.create(spec).await?;

        Ok(Response::new(GenericResponse {
            id,
            state: ContainerStatus::Created.into(),
        }))
    }

    /// Handle starting a container
    #[tracing::instrument]
    async fn start(
        &self,
        request: Request<IdRequest>,
    ) -> Result<Response<GenericResponse>, Status> {
        // Get the inner request data
        let start_request = request.into_inner();

        // Start the container
        self.client.start(&start_request.id).await?;

        Ok(Response::new(GenericResponse {
            id: start_request.id,
            state: ContainerStatus::Running.into(),
        }))
    }

    /// Handle stopping a container
    #[tracing::instrument]
    async fn stop(&self, request: Request<IdRequest>) -> Result<Response<GenericResponse>, Status> {
        // Get the inner request data
        let stop_request = request.into_inner();

        // Stop the container
        self.client.stop(&stop_request.id).await?;

        Ok(Response::new(GenericResponse {
            id: stop_request.id,
            state: ContainerStatus::Stopped.into(),
        }))
    }

    /// Handle retrieving the status of a container
    #[tracing::instrument]
    async fn status(
        &self,
        request: Request<IdRequest>,
    ) -> Result<Response<GenericResponse>, Status> {
        // Get the inner request data
        let status_request = request.into_inner();

        // Retrieve status about the container
        let container = self.client.status(&status_request.id).await?;

        Ok(Response::new(GenericResponse {
            id: status_request.id,
            state: container.state.status.into(),
        }))
    }

    /// Handle retrieving the statistics about a container
    #[tracing::instrument]
    async fn statistics(
        &self,
        request: Request<IdRequest>,
    ) -> Result<Response<StatisticsResponse>, Status> {
        // Get inner request data
        let statistics_request = request.into_inner();

        // Retrieve statistics about the container
        let stats = self.client.statistics(&statistics_request.id).await?;

        Ok(Response::new(StatisticsResponse {
            id: statistics_request.id,
            cpu: stats.cpu_stats.into_protobuf(),
            memory: stats.memory_stats.into_protobuf(),
            pids: stats.pids_stats.into_protobuf(),
            blk_io: stats.blkio_stats.into_protobuf(),
            network: stats.networks.into_protobuf(),
        }))
    }

    #[tracing::instrument]
    async fn logs(&self, request: Request<LogsRequest>) -> Result<Response<LogsResponse>, Status> {
        // Get inner request data
        let logs_request = request.into_inner();

        // Get the list of logs
        let logs = self
            .client
            .logs(&logs_request.id, logs_request.start, logs_request.end)
            .await?;

        Ok(Response::new(LogsResponse {
            id: logs_request.id,
            logs: logs.into_iter().map(|l| l.into_protobuf()).collect(),
        }))
    }

    /// Handle deleting a container
    #[tracing::instrument]
    async fn delete(&self, request: Request<IdRequest>) -> Result<Response<EmptyResponse>, Status> {
        // Get the inner request data
        let delete_request = request.into_inner();

        // Delete the container
        self.client.delete(&delete_request.id).await?;

        // Remove the container's files
        remove_dir_all(self.containers.join(&delete_request.id))
            .await
            .map_err(|e| {
                Status::aborted(format!("failed to delete container filesystem: {}", e))
            })?;

        Ok(Response::new(EmptyResponse {}))
    }

    /// Handle killing a container
    #[tracing::instrument]
    async fn kill(&self, request: Request<IdRequest>) -> Result<Response<EmptyResponse>, Status> {
        // Get the inner request data
        let kill_request = request.into_inner();

        // Kill inner process
        self.client.kill(&kill_request.id).await?;

        Ok(Response::new(EmptyResponse {}))
    }
}
