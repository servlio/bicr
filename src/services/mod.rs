use crate::{config::Config, protobuf::runtime::runtime_server::RuntimeServer};

mod runtime;

/// Initialize the runtime gRPC handlers
pub fn runtime(config: &Config) -> RuntimeServer<runtime::RuntimeService> {
    RuntimeServer::new(runtime::RuntimeService::new(config))
}
