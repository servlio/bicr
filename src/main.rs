#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate log;
#[macro_use]
extern crate prettytable;

use anyhow::{Context, Result};
use std::path::PathBuf;
use structopt::StructOpt;
use tonic::transport::{Identity, Server, ServerTlsConfig};

mod cmd;
mod config;
mod podman;
mod protobuf;
mod services;

use cmd::{handlers, CreateFromLocations, Subcommand};
use protobuf::runtime::binary_source::Location;

#[tokio::main]
async fn main() -> Result<()> {
    // Get command line args
    let args = cmd::Args::from_args();

    // Match on the subcommand
    match args.cmd {
        Subcommand::Daemon { config } => daemon_cmd(config).await?,
        Subcommand::Create { location } => {
            // Extract the arguments for each location
            let (base, pull_from, bucket, region, endpoint) = match location {
                CreateFromLocations::Local(base) => (base, Location::Local, None, None, None),
                CreateFromLocations::GCS { bucket, base } => {
                    (base, Location::Gcs, Some(bucket), None, None)
                }
                CreateFromLocations::S3 {
                    endpoint,
                    bucket,
                    region,
                    base,
                } => (base, Location::S3, Some(bucket), Some(region), endpoint),
            };

            // Execute the command to create the container
            handlers::create(
                args.address,
                pull_from,
                base.image,
                base.destination,
                base.binary,
                base.command,
                bucket,
                region,
                endpoint,
            )
            .await?
        }
        Subcommand::Start(outer) => handlers::start(args.address, outer.id).await?,
        Subcommand::Status(outer) => handlers::status(args.address, outer.id).await?,
        Subcommand::Statistics(outer) => handlers::statistics(args.address, outer.id).await?,
        Subcommand::Logs {
            id,
            start_date,
            start_time,
            end_date,
            end_time,
        } => handlers::logs(args.address, id, start_date, start_time, end_date, end_time).await?,
        Subcommand::Stop(outer) => handlers::stop(args.address, outer.id).await?,
        Subcommand::Kill(outer) => handlers::kill(args.address, outer.id).await?,
        Subcommand::Delete(outer) => handlers::delete(args.address, outer.id).await?,
    }

    Ok(())
}

/// Initialize logging at the given level
fn init_logging(level: tracing::Level, is_json: bool) {
    if is_json {
        tracing_subscriber::fmt()
            .json()
            .with_max_level(level)
            .init();
    } else {
        tracing_subscriber::fmt().with_max_level(level).init();
    }
}

/// Run the engine as a daemon
async fn daemon_cmd(config_path: Option<PathBuf>) -> Result<()> {
    // Parse and validate configuration
    let cfg = config::parse(config_path)
        .await
        .context("Failed to load configuration")?;
    cfg.validate()?;

    init_logging(cfg.logging.level.clone(), cfg.logging.json);

    // Create the base server
    let mut server = Server::builder().trace_fn(|_| tracing::info_span!("bicr"));

    // Add TLS if needed
    if cfg.transport.tls.enabled {
        info!("Enabling TLS support");

        // Read certificates
        let cert = tokio::fs::read(&cfg.transport.tls.cert)
            .await
            .context("Failed to read TLS certificate")?;
        let key = tokio::fs::read(&cfg.transport.tls.key)
            .await
            .context("Failed to read TLS private key")?;

        // Register certificate
        let identity = Identity::from_pem(cert, key);
        server = server.tls_config(ServerTlsConfig::new().identity(identity))?;
    }

    info!("Starting the daemon on {}", &cfg.transport.address);

    // Start the service
    server
        .add_service(services::runtime(&cfg))
        .serve(cfg.transport.address)
        .await?;

    Ok(())
}
