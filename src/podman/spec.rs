use serde::Serialize;
use std::{collections::HashMap, path::PathBuf};

/// Generate a HashMap from key-value pairs
macro_rules! map {
    ( $( $key:expr => $value:expr ),+ ) => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key.into(), $value.into());
            )+
            m
        }
    };
}
#[derive(Debug, Serialize)]
pub struct Spec {
    /// The name the container will be given
    pub name: String,
    /// The command to run in the container
    pub entrypoint: Vec<String>,
    /// A set of environment variables to be set in the container
    pub env: HashMap<String, String>,
    /// Whether the container will create a PTY
    pub terminal: bool,
    /// Whether the container will keep its STDIN open
    pub stdin: bool,
    /// Metadata to add to the container
    pub labels: HashMap<String, String>,
    /// How logging should be handled for the container
    pub log_configuration: Option<LogConfig>,
    /// The container's hostname
    pub hostname: Option<String>,
    /// The container's PID namespace
    #[serde(rename = "pidns")]
    pub pid_namespace: Namespace,
    /// The container's UTS (hostname) namespace
    /// This must be set to private to be able to set the container's hostname
    #[serde(rename = "utsns")]
    pub uts_namespace: Namespace,
    /// Path to the directory to be used as the root filesystem
    pub rootfs: PathBuf,
    /// Additional mount points to be added to the container filesystem
    pub mounts: Vec<Mount>,
    /// The container's IPC namespace
    #[serde(rename = "ipcns")]
    pub ipc_namespace: Namespace,
    /// The container's working directory
    pub work_dir: PathBuf,
    /// The user for the container to run as.
    /// Can be a UID or username; if a username, it will be
    /// resolved using the container's /etc/passwd.
    pub user: Option<String>,
    /// Supplemental groups the container's user will be granted
    pub groups: Vec<String>,
    /// Additional capabilities to be given to the container
    pub cap_add: Vec<String>,
    /// Capabilities to be removed from the container,
    pub cap_drop: Vec<String>,
    /// Prevent the container from gaining new privileges
    /// (e.g. via setuid) in the container.
    pub no_new_privileges: bool,
    /// The container's user namespace. Defaults to host where no
    /// user namespace will be created. If set to private,
    /// id_mappings must be present.
    #[serde(rename = "userns")]
    pub user_namespace: Namespace,
    /// UID and GID mappings for the user namespace
    pub id_mappings: Option<IdMappings>,
    /// The container's networking namespace
    #[serde(rename = "netns")]
    pub network_namespace: Namespace,
    /// The list of CNI networks to join. If none are specified,
    /// the default will be joined. If there is at least one
    /// network, the default will not be joined.
    pub cni_networks: Vec<String>,
    /// Resource limits to apply to the container using cgroups
    pub resource_limits: Option<ResourceLimits>,
    /// Posix rlimits to apply to the container
    pub r_limits: Vec<Rlimit>,
    /// Adjust the score used by OOM killer to determine
    /// which process to kill.
    #[serde(rename = "oom_score_adj")]
    pub oom_score_adjust: Option<isize>,
    /// Container health check configuration
    #[serde(rename = "healthconfig")]
    pub health_config: Option<HealthConfig>,
}
#[derive(Debug, Serialize)]
pub struct LogConfig {
    /// The driver to use for logging
    pub driver: String,
    /// Where the logs should be stored.
    /// Only available for json-file and k8s-file
    pub path: String,
    /// Options to pass to the driver
    pub options: HashMap<String, String>,
}

/// Possible modes for namespaces
#[derive(Debug, Serialize)]
pub enum NamespaceMode {
    /// Allow the namespace to be derived from the host
    #[serde(rename = "host")]
    Host,
    /// Create a new, private namespace
    #[serde(rename = "private")]
    Private,
    /// Use a CNI network stack
    #[serde(rename = "bridge")]
    BridgeNetwork,
}

/// Description for a namespace
#[derive(Debug, Serialize)]
pub struct Namespace {
    #[serde(rename = "nsmode")]
    pub mode: NamespaceMode,
    #[serde(rename = "string")]
    pub value: Option<String>,
}

/// An additional mount point for a container
#[derive(Debug, Serialize)]
pub struct Mount {
    /// The absolute path for where the mount will be placed
    pub destination: String,
    /// The kind of mount
    #[serde(rename = "type")]
    pub mount_type: String,
    /// The source path of the mount
    pub source: String,
    /// Fstab style mount options
    pub options: Vec<String>,
}

/// Options for specifying how IDs should be mapped to groups and users
#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct IdMappings {
    pub host_uid_mapping: bool,
    pub host_gid_mapping: bool,
    pub uid_map: Vec<IdMap>,
    pub gid_map: Vec<IdMap>,
    pub auto_user_ns: bool,
    pub auto_user_ns_opts: AutoUserNsOptions,
}

/// A single entry for the user namespace range remapping
#[derive(Debug, Serialize)]
pub struct IdMap {
    pub container_id: isize,
    pub host_id: isize,
    pub size: isize,
}

/// How to automatically generate a user namespace
#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct AutoUserNsOptions {
    /// The size of the user namespace
    pub size: u32,
    /// The minimum size for the user namespace
    pub initial_size: u32,
    /// The /etc/passwd file to use if using a volume
    pub passwd_file: String,
    /// The /etc/groups file to use if using a volume
    pub group_file: String,
    /// Additional UID mappings to include in the generated namespace
    pub additional_uid_mappings: Vec<IdMap>,
    /// Additional GID mappings to include in the generated namespace
    pub additional_gid_mappings: Vec<IdMap>,
}

/// Container runtime resource constraints
#[derive(Debug, Serialize)]
pub struct ResourceLimits {
    /// Configuration for the devices the container is allowed access to
    pub devices: Vec<DeviceCgroup>,
    /// Memory restriction
    pub memory: Option<Memory>,
    /// CPU resource restriction
    pub cpu: Option<Cpu>,
    /// Task resource restriction
    pub pids: Option<Pids>,
    /// BlockIO restriction configuration
    #[serde(rename = "blockIO")]
    pub block_io: Option<BlockIo>,
    /// HugeTlb limits in bytes
    #[serde(rename = "hugepageLimits")]
    pub hugepage_limits: Vec<HugePageLimit>,
    /// Network restriction configuration
    pub network: Option<Network>,
    /// Rdma resource restriction configuration
    pub rdma: HashMap<String, Rdma>,
    /// Unified resources
    pub unified: HashMap<String, String>,
}

/// A device rule specifying access to the device controller
#[derive(Debug, Serialize)]
pub struct DeviceCgroup {
    /// Allow or deny access to the device
    pub allow: bool,
    /// The device type
    #[serde(rename = "type")]
    pub device_type: Option<String>,
    /// The device's major number
    pub major: Option<i64>,
    /// The device's minor number
    pub minor: Option<i64>,
    /// cgroup access permissions format, i.e. ``rwm``.
    pub access: String,
}

/// Cgroup memory resource management
#[derive(Debug, Serialize)]
pub struct Memory {
    /// Maximum memory that can be used in bytes
    pub limit: Option<i64>,
    /// The reserved amount of memory in bytes
    pub reservation: Option<i64>,
    /// The total memory limit (memory + swap)
    pub swap: Option<i64>,
    /// The kernel memory limit in bytes
    pub kernel: Option<i64>,
    /// The kernel memory limit for TCP in bytes
    #[serde(rename = "kernelTCP")]
    pub kernel_tcp: Option<i64>,
    /// How agressive the kernel should be for swapping memory pages
    pub swappiness: Option<u64>,
    /// Disables the out-of-memory killer
    #[serde(rename = "disableOOMKiller")]
    pub disable_oom_killer: Option<bool>,
    /// Enables hierarchical memory accounting
    #[serde(rename = "useHierarchy")]
    pub use_hierarchy: Option<bool>,
}

/// Cgroup CPU resource management
#[derive(Debug, Serialize)]
pub struct Cpu {
    /// CPU shares (relative weight (ratio) vs other cgroups w/ CPU shares)
    shares: Option<u64>,
    /// The allowed CPU time in a given period as a hard limit in microseconds
    quota: Option<i64>,
    /// The CPU period used for determining the hard limit in microseconds
    period: Option<u64>,
    /// How much time realtime scheduling may use in microseconds
    #[serde(rename = "realtimeRuntime")]
    realtime_runtime: Option<i64>,
    /// The CPU period used for realtime scheduling in microseconds
    #[serde(rename = "realtimePeriod")]
    realtime_period: Option<u64>,
    /// The CPUs to use within the cpuset. Defaults to using any
    /// available CPU.
    pub cpus: String,
    /// List of memory nodes to use in the cpuset. Defaults to using
    /// any available memory node.
    #[serde(rename = "mems")]
    pub memory_nodes: String,
}

/// Cgroup PIDs resource management
#[derive(Debug, Serialize)]
pub struct Pids {
    /// The maximum number of PIDs, defaults to no limit
    pub limit: i64,
}

/// Cgroup block IO resource management
#[derive(Debug, Serialize)]
pub struct BlockIo {
    /// Specifies the per-cgroup weight
    pub weight: Option<u16>,
    /// The bandwidth rate for the device while competing with child cgroups.
    /// This only works with the CFQ scheduler.
    #[serde(rename = "leafWeight")]
    pub leaf_weight: Option<u16>,
    /// The weight given per cgroup per device. This can override ``weight``
    #[serde(rename = "weightDevice")]
    pub weight_device: Vec<BlockIoWeight>,
    /// IO read rate limit per cgroup per device in bytes per second
    #[serde(rename = "throttleReadBpsDevice")]
    pub throttle_read_bps_device: Vec<BlockIoThrottle>,
    /// IO write rate limit per cgroup per device in bytes per second
    #[serde(rename = "throttleWriteBpsDevice")]
    pub throttle_write_bps_device: Vec<BlockIoThrottle>,
    /// IO read rate limit per cgroup per device in IO per second
    #[serde(rename = "throttleReadIOPSDevice")]
    pub throttle_read_iops_device: Vec<BlockIoThrottle>,
    /// IO write rate limit per cgroup per device in IO per second
    #[serde(rename = "throttleWriteIOPSDevice")]
    pub throttle_write_iops_device: Vec<BlockIoThrottle>,
}

/// Throttling specification for an IO device
#[derive(Debug, Serialize)]
pub struct BlockIoThrottle {
    /// The device's major number
    pub major: i64,
    /// The device's minor number
    pub minor: i64,
    /// The IO rate limit per cgroup per device
    pub rate: u64,
}

/// Weight specification for an IO device
#[derive(Debug, Serialize)]
pub struct BlockIoWeight {
    /// The device's major number
    pub major: i64,
    /// The device's minor number
    pub minor: i64,
    /// The bandwidth rate for the device
    pub weight: Option<u16>,
    /// The bandwidth rate for the device while competing with child cgroups.
    /// This only works with the CFQ scheduler.
    #[serde(rename = "leafWeight")]
    pub leaf_weight: Option<u16>,
}

/// The limits for kernel huge pages
#[derive(Debug, Serialize)]
pub struct HugePageLimit {
    /// Size for huge pages. Must be in the format: ``<size><unit-prefix>B``.
    /// For example: 64KB, 2MB, 1GB, etc.
    pub page_size: String,
    /// The limit of HugeTlb usage
    pub limit: u64,
}

/// Network identification and prioritization
#[derive(Debug, Serialize)]
pub struct Network {
    /// Set the class identifier for the container's network packets
    #[serde(rename = "classID")]
    pub class_id: Option<u32>,
    /// Priorities for the network traffic
    pub priorities: Vec<NetworkInterfacePriority>,
}

/// Priorities for different network interfaces
#[derive(Debug, Serialize)]
pub struct NetworkInterfacePriority {
    /// The name of the network interface
    pub name: String,
    /// Priorities for the interface
    pub priority: u32,
}

/// Cgroup RDMA resource management
#[derive(Debug, Serialize)]
pub struct Rdma {
    /// Maximum number of HCA handles that can be opened
    #[serde(rename = "hcaHandles")]
    hca_handles: Option<u32>,
    /// Maximum number of HCA objects that can be created
    #[serde(rename = "hcaObjects")]
    hca_objects: Option<u32>,
}

/// Rlimit types and restrictions
#[derive(Debug, Serialize)]
pub struct Rlimit {
    /// The type of rlimit to set
    #[serde(rename = "type")]
    pub limit_type: String,
    /// The hard limit for the specified type
    pub hard: u64,
    /// The soft limit for the specified type
    pub soft: u64,
}

/// Configuration for container health checks
#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct HealthConfig {
    /// The test to perform within the container.
    /// An empty vec inherits the default healthcheck. The possible options are:
    /// - ``[]`` : inherit
    /// - ``["NONE"]`` : disable healthchecking
    /// - ``["CMD", <args...>]`` : execute arguments directly in the container
    /// - ``["CMD-SHELL", <command>]`` : run the command with the default shell
    pub test: Vec<String>,
    /// The time to wait after starting before the first check in nanoseconds
    pub start_period: u64,
    /// The time to wait between checks in nanoseconds
    pub interval: u64,
    /// The time to wait before considering the check as hung in nanoseconds
    pub timeout: u64,
    /// The number of consecutive failures to consider a container as unhealthy
    pub retries: isize,
}

impl Spec {
    /// Generate a new container specification with the input parameters
    pub fn generate(args: Vec<String>, id: &str, rootfs: &PathBuf) -> Self {
        Self {
            name: id.to_string(),
            entrypoint: args,
            env: map! {
                "PATH" => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "TERM" => "xterm"
            },
            terminal: true,
            stdin: false,
            labels: Default::default(),
            log_configuration: None,
            hostname: Some(id.into()),
            pid_namespace: Namespace {
                mode: NamespaceMode::Private,
                value: None,
            },
            uts_namespace: Namespace {
                mode: NamespaceMode::Private,
                value: None,
            },
            rootfs: rootfs.to_owned(),
            mounts: vec![
                Mount {
                    destination: "/proc".into(),
                    mount_type: "proc".into(),
                    source: "proc".into(),
                    options: vec![],
                },
                Mount {
                    destination: "/dev".into(),
                    mount_type: "tmpfs".into(),
                    source: "tmpfs".into(),
                    options: vec![
                        "nosuid".into(),
                        "strictatime".into(),
                        "mode=755".into(),
                        "size=65536k".into(),
                    ],
                },
                Mount {
                    destination: "/dev/pts".into(),
                    mount_type: "devpts".into(),
                    source: "devpts".into(),
                    options: vec![
                        "nosuid".into(),
                        "noexec".into(),
                        "newinstance".into(),
                        "ptmxmode=0666".into(),
                        "mode=0620".into(),
                        "gid=5".into(),
                    ],
                },
                Mount {
                    destination: "/dev/shm".into(),
                    mount_type: "tmpfs".into(),
                    source: "shm".into(),
                    options: vec!["nosuid".into(), "noexec".into(), "nodev".into()],
                },
                Mount {
                    destination: "/sys".into(),
                    mount_type: "sysfs".into(),
                    source: "sysfs".into(),
                    options: vec![
                        "nosuid".into(),
                        "noexec".into(),
                        "nodev".into(),
                        "ro".into(),
                    ],
                },
                Mount {
                    destination: "/sys/fs/cgroup".into(),
                    mount_type: "cgroup".into(),
                    source: "cgroup".into(),
                    options: vec![
                        "nosuid".into(),
                        "noexec".into(),
                        "nodev".into(),
                        "relatime".into(),
                        "ro".into(),
                    ],
                },
            ],
            ipc_namespace: Namespace {
                mode: NamespaceMode::Private,
                value: None,
            },
            work_dir: "/".into(),
            user: None,
            groups: vec![],
            cap_add: vec![
                "CAP_AUDIT_WRITE".into(),
                "CAP_KILL".into(),
                "CAP_NET_BIND_SERVICE".into(),
                "CAP_NET_RAW".into(),
            ],
            cap_drop: vec![],
            no_new_privileges: true,
            user_namespace: Namespace {
                mode: NamespaceMode::Host,
                value: None,
            },
            id_mappings: None,
            network_namespace: Namespace {
                mode: NamespaceMode::BridgeNetwork,
                value: None,
            },
            cni_networks: vec![],
            resource_limits: Some(ResourceLimits {
                devices: vec![DeviceCgroup {
                    allow: false,
                    device_type: None,
                    major: None,
                    minor: None,
                    access: "rwm".to_string(),
                }],
                memory: None,
                cpu: None,
                pids: None,
                block_io: None,
                hugepage_limits: vec![],
                network: None,
                rdma: Default::default(),
                unified: Default::default(),
            }),
            r_limits: vec![],
            oom_score_adjust: None,
            health_config: None,
        }
    }
}
