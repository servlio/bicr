use crate::protobuf::runtime::{
    statistics as protobuf, Log as ProtobufLog, State as ProtobufState,
};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// Allow converting to the corresponding protobuf message.
/// This is primarily for the statistics response.
pub trait IntoProtobuf<T> {
    fn into_protobuf(self) -> T;
}

/// Generic error data
#[derive(Deserialize, Debug)]
pub struct Error {
    pub cause: String,
    pub message: String,
    pub response: i64,
}

/// Log response data
#[derive(Debug, Serialize)]
#[serde(tag = "type", content = "content", rename_all = "lowercase")]
pub enum Log {
    Stdout(String),
    Stderr(String),
    Unknown(String),
}

impl Log {
    /// Create a new log entry from the given device type
    pub(crate) fn new(t: u8, data: String) -> Self {
        match t {
            1 => Log::Stdout(data),
            2 => Log::Stderr(data),
            _ => {
                warn!("unknown device '{}' in log from container", t);
                Log::Unknown(data)
            }
        }
    }
}

impl IntoProtobuf<ProtobufLog> for Log {
    fn into_protobuf(self) -> ProtobufLog {
        let (t, content) = match self {
            Self::Stdout(s) => (0, s),
            Self::Stderr(s) => (1, s),
            Self::Unknown(s) => (2, s),
        };

        ProtobufLog { content, r#type: t }
    }
}

/// Create response data
#[derive(Deserialize, Debug)]
pub struct Create {
    #[serde(rename = "Id")]
    pub id: String,
    #[serde(rename = "Warnings")]
    pub warnings: Vec<String>,
}

/// Inspect response data
#[derive(Deserialize, Debug)]
pub struct Inspect {
    #[serde(rename = "Id")]
    pub internal_id: String,
    #[serde(rename = "Name")]
    pub id: String,
    #[serde(rename = "NetworkSettings")]
    pub network: InspectNetwork,
    #[serde(rename = "State")]
    pub state: InspectState,
}

/// Network response data within the inspect response
#[derive(Deserialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct InspectNetwork {
    pub bridge: Option<String>,
    pub ip_address: Option<String>,
    pub gateway: Option<String>,
    pub ports: HashMap<String, Vec<NetworkPorts>>,
}

/// Port definition that is forwarded
#[derive(Deserialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct NetworkPorts {
    pub host_ip: String,
    pub host_port: String,
}

/// Container state response data withing the inspect response
#[derive(Deserialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct InspectState {
    pub dead: bool,
    pub exit_code: i32,
    #[serde(rename = "OOMKilled")]
    pub oom_killed: bool,
    pub paused: bool,
    pub restarting: bool,
    pub running: bool,
    pub status: ContainerStatus,
}

/// Current state of the container
#[derive(Deserialize, Debug)]
pub enum ContainerStatus {
    #[serde(rename = "created")]
    #[serde(alias = "configured")]
    Created,
    #[serde(rename = "running")]
    Running,
    #[serde(rename = "stopped")]
    #[serde(alias = "exited")]
    #[serde(alias = "paused")]
    Stopped,
}

impl Into<i32> for ContainerStatus {
    fn into(self) -> i32 {
        match self {
            Self::Created => ProtobufState::Created.into(),
            Self::Running => ProtobufState::Running.into(),
            Self::Stopped => ProtobufState::Stopped.into(),
        }
    }
}

/// Statistics response data
#[derive(Deserialize, Debug)]
pub struct Statistics {
    pub read: DateTime<Utc>,
    pub pids_stats: StatisticsPids,
    pub blkio_stats: StatisticsBlkio,
    pub cpu_stats: StatisticsCpu,
    pub memory_stats: StatisticsMemory,
    pub networks: StatisticsNetwork,
    pub name: String,
    #[serde(rename = "Id")]
    pub id: String,
}

#[derive(Deserialize, Debug)]
pub struct StatisticsPids {
    pub current: Option<u64>,
    pub limit: Option<u64>,
}

impl IntoProtobuf<protobuf::Pids> for StatisticsPids {
    fn into_protobuf(self) -> protobuf::Pids {
        protobuf::Pids {
            current: self.current.unwrap_or_default(),
            limit: self.limit,
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct StatisticsBlkio {
    pub io_service_bytes_recursive: StatisticsBlkioEntries,
}

impl IntoProtobuf<protobuf::Blkio> for StatisticsBlkio {
    fn into_protobuf(self) -> protobuf::Blkio {
        protobuf::Blkio {
            io_service_bytes_recursive: self.io_service_bytes_recursive.into_protobuf(),
        }
    }
}

type StatisticsBlkioEntries = Vec<StatisticsBlkioEntry>;

impl IntoProtobuf<Vec<protobuf::BlkioEntry>> for StatisticsBlkioEntries {
    fn into_protobuf(self) -> Vec<protobuf::BlkioEntry> {
        self.iter()
            .map(|e| protobuf::BlkioEntry {
                major: e.major,
                minor: e.minor,
                op: e.op.clone(),
                value: e.value,
            })
            .collect()
    }
}

#[derive(Deserialize, Debug)]
pub struct StatisticsBlkioEntry {
    pub major: u64,
    pub minor: u64,
    pub op: String,
    pub value: u64,
}

#[derive(Deserialize, Debug)]
pub struct StatisticsCpu {
    pub cpu_usage: StatisticsCpuUsage,
    pub system_cpu_usage: Option<u64>,
    pub online_cpus: Option<u32>,
    pub cpu: f64,
    pub throttling_data: Option<StatisticsCpuThrottling>,
}

impl IntoProtobuf<protobuf::Cpu> for StatisticsCpu {
    fn into_protobuf(self) -> protobuf::Cpu {
        protobuf::Cpu {
            usage: protobuf::CpuUsage {
                total: self.cpu_usage.total_usage,
                per_cpu: self.cpu_usage.percpu_usage,
                kernel: self.cpu_usage.usage_in_kernelmode,
                user: self.cpu_usage.usage_in_usermode,
            },
            system_cpu_usage: self.system_cpu_usage,
            online_cpus: self.online_cpus,
            cpu: self.cpu,
            throttling: self.throttling_data.into_protobuf(),
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct StatisticsCpuUsage {
    pub total_usage: u64,
    pub percpu_usage: Vec<u64>,
    pub usage_in_kernelmode: u64,
    pub usage_in_usermode: u64,
}

type StatisticsCpuThrottlingWrapper = Option<StatisticsCpuThrottling>;
impl IntoProtobuf<Option<protobuf::Throttling>> for StatisticsCpuThrottlingWrapper {
    fn into_protobuf(self) -> Option<protobuf::Throttling> {
        self.map(|i| protobuf::Throttling {
            periods: i.periods,
            throttled_periods: i.throttled_periods,
            throttled_time: i.throttled_time,
        })
    }
}

#[derive(Deserialize, Debug)]
pub struct StatisticsCpuThrottling {
    pub periods: u64,
    pub throttled_periods: u64,
    pub throttled_time: u64,
}

#[derive(Deserialize, Debug)]
pub struct StatisticsMemory {
    pub usage: Option<u64>,
    pub max_usage: Option<u64>,
    pub stats: Option<HashMap<String, u64>>,
    #[serde(rename = "failcnt")]
    pub fail_count: Option<u64>,
    pub limit: Option<u64>,
    pub commit: Option<u64>,
    #[serde(rename = "commitpeakbytes")]
    pub commit_peak_bytes: Option<u64>,
    #[serde(rename = "privateworkingset")]
    pub private_working_set: Option<u64>,
}

impl IntoProtobuf<protobuf::Memory> for StatisticsMemory {
    fn into_protobuf(self) -> protobuf::Memory {
        protobuf::Memory {
            usage: self.usage,
            max_usage: self.max_usage,
            stats: self.stats.unwrap_or_default(),
            fail_count: self.fail_count,
            limit: self.limit,
            commit: self.commit,
            commit_peak_bytes: self.commit_peak_bytes,
            private_working_set: self.private_working_set,
        }
    }
}

type StatisticsNetwork = HashMap<String, StatisticsNetworkEntry>;

impl IntoProtobuf<HashMap<String, protobuf::Network>> for StatisticsNetwork {
    fn into_protobuf(self) -> HashMap<String, protobuf::Network> {
        self.iter()
            .map(|(k, v)| {
                (
                    k.to_string(),
                    protobuf::Network {
                        rx_bytes: v.rx_bytes,
                        rx_packets: v.rx_packets,
                        rx_errors: v.rx_errors,
                        rx_dropped: v.rx_dropped,
                        tx_bytes: v.tx_bytes,
                        tx_packets: v.tx_packets,
                        tx_errors: v.tx_errors,
                        tx_dropped: v.tx_dropped,
                    },
                )
            })
            .collect()
    }
}

#[derive(Deserialize, Debug)]
pub struct StatisticsNetworkEntry {
    pub rx_bytes: u64,
    pub rx_packets: u64,
    pub rx_errors: u64,
    pub rx_dropped: u64,
    pub tx_bytes: u64,
    pub tx_packets: u64,
    pub tx_errors: u64,
    pub tx_dropped: u64,
}
