use bytes::{buf::BufExt, Buf};
use http::{method::Method, request::Request, StatusCode};
use hyper::{Body, Client, Response, Uri};
use hyperlocal::{UnixConnector, Uri as LocalUri};
use serde::{de::DeserializeOwned, Serialize};
use std::path::{Path, PathBuf};
use tonic::Status;

mod responses;
mod spec;

pub use responses::{ContainerStatus, Error, IntoProtobuf};
pub use spec::Spec;

type Result<T> = std::result::Result<T, Status>;

/// Interact with the Podman HTTP API to control the containers
#[derive(Debug)]
pub struct Podman {
    client: Client<UnixConnector>,
    base: PathBuf,
}

impl Podman {
    /// Initializes a new Podman connector at the given URL
    pub fn new<P: AsRef<Path>>(base: P) -> Self {
        Self {
            client: Client::builder().build(UnixConnector),
            base: base.as_ref().into(),
        }
    }

    /// Create a URI for the socket
    fn build_uri<S: AsRef<str>>(&self, path: S) -> Uri {
        LocalUri::new(&self.base, path.as_ref()).into()
    }

    /// Build a generic request
    fn request<S: AsRef<str>>(&self, method: Method, path: S) -> Result<Request<Body>> {
        Ok(Request::builder()
            .method(method)
            .uri(self.build_uri(path))
            .body(Body::from(vec![]))
            .map_err(|_| Status::unknown("failed to build request"))?)
    }

    /// Build a request with a JSON body
    fn request_with_body<B: Serialize + ?Sized, S: AsRef<str>>(
        &self,
        method: Method,
        path: S,
        body: &B,
    ) -> Result<Request<Body>> {
        // Serialize body to JSON
        let serialized = serde_json::to_vec(body).map_err(|e| {
            Status::internal(format!("unable to convert request body to JSON: {}", e))
        })?;

        // Build request
        let request = Request::builder()
            .method(method)
            .uri(self.build_uri(path))
            .body(Body::from(serialized))
            .map_err(|_| Status::unknown("failed to build request"))?;

        Ok(request)
    }

    /// Deserialize a response
    async fn deserialize_response<T: DeserializeOwned>(res: Response<Body>) -> Result<T> {
        // Get the entire body
        let body = hyper::body::aggregate(res)
            .await
            .map_err(|_| Status::internal("failed to read body"))?;

        // Parse the JSON
        Ok(serde_json::from_reader::<_, T>(body.reader())
            .map_err(|e| Status::internal(format!("failed to parse JSON: {}", e)))?)
    }

    /// Extract the human readable error response from the body
    async fn parse_error_response(res: Response<Body>) -> Result<Error> {
        Self::deserialize_response::<Error>(res).await
    }

    /// Create a new container from the specified root filesystem and configuration.
    pub async fn create(&self, spec: Spec) -> Result<String> {
        // Build the request
        let req =
            self.request_with_body(Method::POST, "/v1.0.0/libpod/containers/create", &spec)?;

        // Send the request
        let raw_response = self
            .client
            .request(req)
            .await
            .map_err(|e| Status::aborted(format!("failed to execute request: {}", e)))?;

        // Check response status code
        if raw_response.status() != StatusCode::CREATED {
            let error = Self::parse_error_response(raw_response).await?;
            error!(
                "failed to create container: ({}) {}",
                error.response, error.message
            );
            return Err(Status::internal("failed to create container"));
        }

        // Parse the response
        let response: responses::Create = Self::deserialize_response(raw_response).await?;

        // Log any warnings to console
        for warning in response.warnings {
            warn!("warning raised while creating container: {}", warning);
        }

        Ok(response.id)
    }

    /// Start a container with the provided id.
    pub async fn start(&self, id: &str) -> Result<()> {
        // Build the request
        let req = self.request(
            Method::POST,
            format!("/v1.0.0/libpod/containers/{}/start", id),
        )?;

        // Send the request
        let response = self
            .client
            .request(req)
            .await
            .map_err(|e| Status::aborted(format!("failed to execute request: {}", e)))?;

        // Check the status code
        if response.status() == StatusCode::NO_CONTENT {
            Ok(())
        } else if response.status() == StatusCode::NOT_MODIFIED {
            Err(Status::failed_precondition("container already started"))
        } else if response.status() == StatusCode::NOT_FOUND {
            Err(Status::not_found("container does not exist"))
        } else {
            let error = Self::parse_error_response(response).await?;
            error!(
                "failed to start container: ({}) {}",
                error.response, error.message
            );
            Err(Status::unknown("failed to start container"))
        }
    }

    /// Get the current status of a container.
    pub async fn status(&self, id: &str) -> Result<responses::Inspect> {
        // Build the request
        let req = self.request(
            Method::GET,
            format!("/v1.0.0/libpod/containers/{}/json", id),
        )?;

        // Send the request
        let raw_response = self
            .client
            .request(req)
            .await
            .map_err(|e| Status::aborted(format!("failed to execute request: {}", e)))?;

        // Check response status code
        if raw_response.status() == StatusCode::NOT_FOUND {
            return Err(Status::not_found("container does not exist"));
        } else if raw_response.status() != StatusCode::OK {
            let error = Self::parse_error_response(raw_response).await?;
            error!(
                "failed to create container: ({}) {}",
                error.response, error.message
            );
            return Err(Status::unknown("failed to get container status"));
        }

        // Parse the response
        Ok(Self::deserialize_response(raw_response).await?)
    }

    /// Get runtime statistics about the container.
    pub async fn statistics(&self, id: &str) -> Result<responses::Statistics> {
        // Build the request
        let req = self.request(
            Method::GET,
            format!("/v1.0.0/libpod/containers/{}/stats?stream=false", id),
        )?;

        // Send the request
        let raw_response = self
            .client
            .request(req)
            .await
            .map_err(|e| Status::aborted(format!("failed to execute request: {}", e)))?;

        // Check the response status code
        if raw_response.status() == StatusCode::NOT_FOUND {
            return Err(Status::not_found("container does not exist"));
        } else if raw_response.status() == StatusCode::CONFLICT {
            return Err(Status::failed_precondition("container is not running"));
        } else if raw_response.status() != StatusCode::OK {
            let error = Self::parse_error_response(raw_response).await?;
            error!(
                "failed to create container: ({}) {}",
                error.response, error.message
            );
            return Err(Status::unknown("failed to get container statistics"));
        }

        // Parse the response
        Ok(Self::deserialize_response(raw_response).await?)
    }

    /// Get the specified container's logs in a given timeframe.
    pub async fn logs(
        &self,
        id: &str,
        start: Option<prost_types::Timestamp>,
        end: Option<prost_types::Timestamp>,
    ) -> Result<Vec<responses::Log>> {
        // Build the request path
        let path = {
            // The initial path
            let mut base = format!(
                "/v1.0.0/libpod/containers/{}/logs?stdout=true&stderr=true",
                id
            );

            // For checking if the date range is valid
            let mut diff = std::i64::MAX;

            // The start date to read logs from
            if let Some(start) = start {
                base.push_str("&since=");
                base.push_str(&start.seconds.to_string());
                diff = start.seconds;
            }

            // The end date to read logs to
            if let Some(end) = end {
                base.push_str("&until=");
                base.push_str(&end.seconds.to_string());
                diff -= end.seconds;
            }

            // Ensure time range is valid
            if diff <= 0 {
                return Err(Status::invalid_argument("invalid range for logs"));
            }

            base
        };

        // Build the request
        let req = self.request(Method::GET, path)?;

        // Send the request
        let response = self
            .client
            .request(req)
            .await
            .map_err(|e| Status::aborted(format!("failed to execute request: {}", e)))?;

        // Check response status code
        if response.status() == StatusCode::NOT_FOUND {
            return Err(Status::not_found("container does not exist"));
        } else if response.status() != StatusCode::OK {
            let error = Self::parse_error_response(response).await?;
            error!(
                "failed to create container: ({}) {}",
                error.response, error.message
            );
            return Err(Status::unknown("failed to get container logs"));
        }

        // Read the entire body
        let mut body = hyper::body::to_bytes(response)
            .await
            .map_err(|_| Status::internal("failed to read body"))?;

        // Output array of logs
        let mut logs = vec![];

        // Parse the body format
        while body.has_remaining() {
            // Get the device type and message length
            let stdio_type = body.get_u8();
            body.advance(3);
            let length = body.get_u32() as usize;

            // Get the string
            let raw = &body.bytes()[0..length];
            let converted = String::from_utf8_lossy(raw).to_string();

            // Append to log output with given device
            logs.push(responses::Log::new(stdio_type, converted));

            // Skip length of log
            body.advance(length);
        }

        Ok(logs)
    }

    /// Stop the specified container.
    pub async fn stop(&self, id: &str) -> Result<()> {
        // Build the request
        let req = self.request(
            Method::POST,
            format!("/v1.0.0/libpod/containers/{}/stop?t=3", id),
        )?;

        // Send the request
        let response = self
            .client
            .request(req)
            .await
            .map_err(|e| Status::aborted(format!("failed to execute request: {}", e)))?;

        // Check the status code
        match response.status() {
            StatusCode::NO_CONTENT => Ok(()),
            StatusCode::NOT_MODIFIED => {
                Err(Status::failed_precondition("container already stopped"))
            }
            StatusCode::NOT_FOUND => Err(Status::not_found("container does not exist")),
            _ => Err(Status::unknown("failed to stop container")),
        }
    }

    /// Delete the specified container and its filesystem.
    pub async fn delete(&self, id: &str) -> Result<()> {
        // Build the request
        let req = self.request(Method::DELETE, format!("/v1.0.0/libpod/containers/{}", id))?;

        // Send the request
        let response = self
            .client
            .request(req)
            .await
            .map_err(|e| Status::aborted(format!("failed to execute request: {}", e)))?;

        // Check the status code
        match response.status() {
            StatusCode::NO_CONTENT => Ok(()),
            StatusCode::NOT_FOUND => Err(Status::not_found("container does not exist")),
            _ => Err(Status::unknown("failed to delete container")),
        }
    }

    /// Kill the specified container with a SIGTERM.
    pub async fn kill(&self, id: &str) -> Result<()> {
        // Build the request
        let req = self.request(
            Method::POST,
            format!("/v1.0.0/libpod/containers/{}/kill", id),
        )?;

        // Send the request
        let response = self
            .client
            .request(req)
            .await
            .map_err(|e| Status::aborted(format!("failed to execute request: {}", e)))?;

        // Check the status code
        match response.status() {
            StatusCode::NO_CONTENT => Ok(()),
            StatusCode::NOT_FOUND => Err(Status::not_found("container does not exist")),
            _ => Err(Status::unknown("failed to kill container")),
        }
    }
}
