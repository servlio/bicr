/// The generated protobuf for /proto/runtime.proto
pub mod runtime {
    tonic::include_proto!("runtime");

    pub mod statistics {
        tonic::include_proto!("runtime.statistics");
    }
}
