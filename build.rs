fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure()
        .build_client(true)
        .build_server(true)
        .compile(
            &["proto/runtime.proto", "proto/runtime_statistics.proto"],
            &["proto"],
        )?;
    Ok(())
}
